<?php

namespace App\Helpers;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Trait Responses
 * @package App\Helpers
 */
trait Responses
{
    /**
     * @param $data
     * @param int $code
     * @return JsonResponse
     */
    public function successResponseWithData($data, $code = Response::HTTP_OK)
    {
        return response()->json(
            [
                'status' => true,
                'data' => $data
            ],
            $code
        );
    }

    /**
     * @param int $code
     * @return JsonResponse
     */
    public function successResponse($code = Response::HTTP_OK)
    {
        return response()->json(['status' => true], $code);
    }

    /**
     * @param $messages
     * @param int $code
     */
    public function exceptionResponse($messages, $code = Response::HTTP_OK)
    {
        $response = response()->json(
            [
                'status' => false,
                'messages' => $messages
            ],
            $code
        );
        throw new HttpResponseException($response);
    }
}
