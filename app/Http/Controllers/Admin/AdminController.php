<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Responses;
use App\Http\Controllers\Controller;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
    use Responses;
}
