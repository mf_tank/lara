<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Requests\NewsSearchRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/**
 * Class CategoriesController
 * @package App\Http\Controllers\Admin
 */
class CategoriesController extends AdminController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $categories = Category::paginate(10);


        return view('admin.categories.index', [
            'categories' => $categories
        ]);
    }

    /**
     * @param NewsSearchRequest $newsSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.categories.create', [
            'categories' => $categories
        ]);
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Category $category)
    {
        $categories = Category::all();
        return view('admin.categories.update', [
            'category' => $category,
            'categories' => $categories
        ]);
    }

    /**
     * @param CategoryStoreRequest $categoryStoreRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryStoreRequest $categoryStoreRequest)
    {
        $category = new Category();

        $data = $categoryStoreRequest->validated();

        $category->fill($data);

        $category->save();

        return Redirect::to('/admin/categories');

    }

    /**
     * @param CategoryUpdateRequest $categoryUpdateRequest
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryUpdateRequest $categoryUpdateRequest, Category $category)
    {
        $data = $categoryUpdateRequest->validated();
        $category->fill($data);

        $category->save();

        return Redirect::to('/admin/categories');

    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Category $category)
    {
        return view('admin.categories.show', [
            'category' => $category
        ]);
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        if($category->newses()->count()) {
            Session::flash('error', 'Есть связанные новости');
        } else if($category->children()->count()) {
            Session::flash('error', 'Есть связанные категории');
        } else {
            $category->delete();
            Session::flash('message', 'Успешно удалено');
        }

        return Redirect::to('/admin/categories');
    }

}
