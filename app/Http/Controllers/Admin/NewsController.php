<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsSearchRequest;
use App\Http\Requests\NewsStoreRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\NewsCategory;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/**
 * Class NewsController
 * @package App\Http\Controllers\Api
 */
class NewsController extends AdminController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param NewsSearchRequest $newsSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $newses = News::paginate(10);


        return view('admin.news.index', [
            'newses' => $newses
        ]);
    }

    /**
     * @param NewsSearchRequest $newsSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.news.create', [
            'categories' => $categories
        ]);
    }

    /**
     * @param NewsCreateRequest $newsCreateRequest
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(News $news)
    {
        $categories = Category::all();

        return view('admin.news.update', [
            'news' => $news,
            'categories' => $categories
        ]);
    }

    /**
     * @param NewsStoreRequest $newsStoreRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsStoreRequest $newsStoreRequest)
    {
        $news = new News();

        $data = $newsStoreRequest->validated();

        $news->fill($data);

        if($newsStoreRequest->hasFile('preview_img')) {
            $file = $newsStoreRequest->file('preview_img');
            $file->move(public_path() . '/img/news/', $file->getClientOriginalName());

            $news->preview_img = '/img/news/'.$file->getClientOriginalName();
        }

        if($newsStoreRequest->hasFile('detail_img')) {
            $file = $newsStoreRequest->file('detail_img');
            $file->move(public_path() . '/img/news/', $file->getClientOriginalName());

            $news->detail_img = '/img/news/'.$file->getClientOriginalName();
        }

        $news->save();

        if(!empty($data['categories'])) {
            foreach ($data['categories'] as $id) {
                $category = new NewsCategory();
                $category->news_id = $news->id;
                $category->category_id = $id;
                $category->save();
            }
        }

        return Redirect::to('/admin/news/');

    }

    /**
     * @param NewsUpdateRequest $newsUpdateRequest
     * @param News $news
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(NewsUpdateRequest $newsUpdateRequest, News $news)
    {
        $data = $newsUpdateRequest->validated();
        $news->fill($data);

        if($newsUpdateRequest->hasFile('preview_img')) {
            $file = $newsUpdateRequest->file('preview_img');
            $file->move(public_path() . '/img/news/', $file->getClientOriginalName());

            if(file_exists(public_path().$news->preview_img)) {
                unlink(public_path() . $news->preview_img);
            }

            $news->preview_img = '/img/news/'.$file->getClientOriginalName();
        }

        if($newsUpdateRequest->hasFile('detail_img')) {
            $file = $newsUpdateRequest->file('detail_img');
            $file->move(public_path() . '/img/news/', $file->getClientOriginalName());

            if(file_exists(public_path().$news->detail_img)) {
                unlink(public_path().$news->detail_img);
            }


            $news->detail_img = '/img/news/'.$file->getClientOriginalName();
        }

        $news->save();

        if(!empty($data['categories'])) {
            NewsCategory::where('news_id', $news->id)->delete();

            foreach ($data['categories'] as $id) {
                $category = new NewsCategory();
                $category->news_id = $news->id;
                $category->category_id = $id;
                $category->save();
            }
        }

        return Redirect::to('/admin/news/');

    }

    /**
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(News $news)
    {
        return view('admin.news.show', [
            'news' => $news
        ]);
    }


    public function destroy(News $news)
    {

        $news->categories()->delete();

        if(file_exists(public_path().$news->preview_img)) {
            unlink(public_path() . $news->preview_img);
        }

        if(file_exists(public_path().$news->detail_img)) {
            unlink(public_path().$news->detail_img);
        }

        $news->delete();

        Session::flash('message', 'Успешно удалено');
        return Redirect::to('/admin/news');
    }

}
