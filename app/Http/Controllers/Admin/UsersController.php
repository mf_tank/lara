<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsSearchRequest;
use App\Http\Requests\UserPasswordRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/**
 * Class UsersController
 * @package App\Http\Controllers\Admin
 */
class UsersController extends AdminController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $users = User::paginate(10);

        return view('admin.users.index', [
            'users' => $users
        ]);
    }

    /**
     * @param NewsSearchRequest $newsSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create', [
            'roles' => $roles
        ]);
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(User $user)
    {
        $roles = Role::all();
        return view('admin.users.update', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * @param UserStoreRequest $userStoreRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserStoreRequest $userStoreRequest)
    {
        $user = new User();

        $data = $userStoreRequest->validated();

        $user->fill($data);
        $user->password = Hash::make($user->password);

        $user->save();

        if(!empty($data['roles'])) {
            foreach ($data['roles'] as $id) {
                $role = Role::where('id', $id)->first();

                $user->roles()->attach($role);
            }
        }

        return Redirect::to('/admin/users');

    }

    /**
     * @param UserUpdateRequest $userUpdateRequest
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $userUpdateRequest, User $user)
    {
        $data = $userUpdateRequest->validated();

        $user->fill($data);
        $user->password = Hash::make($user->password);

        $user->save();

        Session::flash('message', 'Успешно обновлено');

        return Redirect::to('/admin/users');

    }

    /**
     * @param UserPasswordRequest $userPasswordRequest
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function roles(UserPasswordRequest $userPasswordRequest, User $user)
    {
        $data = $userPasswordRequest->validated();

        if(!empty($data['roles'])) {
            UserRole::where('user_id', $user->id)->delete();

            foreach ($data['roles'] as $id) {
                $role = Role::where('id', $id)->first();

                $user->roles()->attach($role);
            }
        }
        Session::flash('message', 'Успешно обновлено');

        return Redirect::to('/admin/users');

    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        return view('admin.users.show', [
            'user' => $user
        ]);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();
        Session::flash('message', 'Успешно удалено');

        return Redirect::to('/admin/users');
    }

}
