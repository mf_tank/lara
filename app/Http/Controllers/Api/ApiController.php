<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Responses;
use App\Http\Controllers\Controller;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    use Responses;
}
