<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\NewsSearchRequest;
use App\Http\Resources\NewsResource;
use App\Models\News;
use App\Services\NewsService;
use Facade\FlareClient\Http\Exceptions\BadResponseCode;

/**
 * Class NewsesController
 * @package App\Http\Controllers
 */
class NewsesController extends ApiController
{

    /**
     * @param NewsSearchRequest $newsSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(NewsSearchRequest $newsSearchRequest, NewsService $newsService)
    {
        $response = $newsService->_list($newsSearchRequest);
        return $this->successResponseWithData($response);
    }

    /**
     * @param News $news
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(News $news)
    {
        return $this->successResponseWithData(new NewsResource($news));
    }

}
