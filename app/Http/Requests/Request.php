<?php

namespace App\Http\Requests;

use App\Helpers\Responses;
use App\Reference\Constants;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

/**
 * Class Request
 * @package App\Http\Requests
 */
class Request extends FormRequest
{
    use Responses;

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $this->exceptionResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @brief Запуск до валидации
     */
    protected function prepareForValidation()
    {
        if (!$this->limit) {
            $this->limit = Constants::DEFAULT_LIMIT;
            $this->offset = Constants::DEFAULT_OFFSET;
        }
    }
}
