<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NewsResource
 * @package App\Http\Resources
 */
class NewsResource extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'preview_img' => $this->preview_img,
            'preview_text' => $this->preview_text,
            'detail_img' => $this->detail_img,
            'detail_text' => $this->detail_text,
        ];
    }
}
