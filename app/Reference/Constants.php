<?php

namespace App\Reference;

/**
 * Class Constants
 * @package App\Reference
 */
class Constants
{
    /** @var int  */
    const DEFAULT_LIMIT = 10;
    /** @var int  */
    const DEFAULT_OFFSET = 0;
}
