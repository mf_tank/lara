<?php

namespace App\Services;

use App\Http\Requests\NewsSearchRequest;
use App\Http\Resources\NewsResource;
use App\Models\News;

/**
 * Class NewsService
 * @package App\Services
 */
class NewsService extends Service
{

    /**
     * @param NewsSearchRequest $request
     * @return array
     */
    public function _list(NewsSearchRequest $request)
    {
        $query = News::query()->orderBy('created_at', 'desc');

        $queryCount = $query->count();

        $query->limit($request->limit)
            ->offset($request->offset);

        return [
            'list' => NewsResource::collection($query->get()),
            'listCount' => $queryCount
        ];
    }

}
