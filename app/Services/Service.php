<?php

namespace App\Services;

use App\Helpers\Responses;

/**
 * Class Service
 * @package App\Services
 */
abstract class Service
{
    use Responses;

}
