<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->string('name')->unique();

            $table->foreign('parent_id')->references('id')->on('category');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('news_category', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('news_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();

            $table->foreign('news_id')->references('id')->on('news');
            $table->foreign('category_id')->references('id')->on('category');
            $table->unique(['news_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
