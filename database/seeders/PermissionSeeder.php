<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $permissions = [
            ['slug' => 'view-user', 'name' => 'Просмотр пользователей'],
            ['slug' => 'create-user', 'name' => 'Создание пользователй'],
            ['slug' => 'update-user', 'name' => 'Обновление пользователй'],
            ['slug' => 'delete-user', 'name' => 'Удаление пользователй'],

            ['slug' => 'view-news', 'name' => 'Просмотр новостей'],
            ['slug' => 'create-news', 'name' => 'Создание новостей'],
            ['slug' => 'update-news', 'name' => 'Обновление новостей'],
            ['slug' => 'delete-news', 'name' => 'Удаление нововостей'],

            ['slug' => 'view-category', 'name' => 'Просмотр категорий'],
            ['slug' => 'create-category', 'name' => 'Создание категорий'],
            ['slug' => 'update-category', 'name' => 'Обновление категорий'],
            ['slug' => 'delete-category', 'name' => 'Удаление категорий'],
        ];
        foreach ($permissions as $item) {
            $permission = new Permission();
            $permission->name = $item['name'];
            $permission->slug = $item['slug'];
            $permission->save();
        }
        */
    }
}
