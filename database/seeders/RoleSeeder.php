<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

/**
 * Class RoleSeeder
 * @package Database\Seeders
 */
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->slug = 'admin';
        $role->save();

        $role = new Role();
        $role->name = 'Журналист';
        $role->slug = 'journalist';
        $role->save();

        $role = new Role();
        $role->name = 'Пользователь (блоггер)';
        $role->slug = 'blogger';
        $role->save();

        $role = new Role();
        $role->name = 'Редактор';
        $role->slug = 'editor';
        $role->save();

        $role = new Role();
        $role->name = 'Выкладывающий редактор';
        $role->slug = 'layout_editor';
        $role->save();

        $role = new Role();
        $role->name = 'Верстальщик-дизайнер';
        $role->slug = 'layout_designer';
        $role->save();

    }
}
