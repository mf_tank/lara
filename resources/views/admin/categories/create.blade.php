@extends('adminlte::page')

@section('title', 'Создание категории')

@section('content_header')
    <h1>Создание категории</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('categories.store') }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf

        <div class="form-group">
            <label for="name">
                Название
            </label>

            <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{old('name')}}"/>
        </div>

        @if (count($categories) > 0)
            <div class="form-group">
                <label for="detail_text">
                    Родительская категория
                </label>
                <br/>
                <select name="parent_id">
                    <option></option>
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">
                            {{$category->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif


        <button type="submit" class="btn btn-primary">
            Создать
        </button>

    </form>

@endsection
