@extends('adminlte::page')

@section('title', $category->name)

@section('content_header')
    <h1>{{$category->name}}</h1>
@stop

@section('content')

    @if($category->parent)
        <h2>
            Родитель
        </h2>

        <div>
            {{$category->parent->name}}
        </div>
    @endif

@endsection
