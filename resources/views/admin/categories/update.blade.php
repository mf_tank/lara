@extends('adminlte::page')

@section('title', 'Редактирование категории')

@section('content_header')
    <h1>Редактирование категории</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('categories.update', $category->id) }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">
                Название
            </label>

            <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{$category->name}}"/>
        </div>

        @if (count($categories) > 0)
            <div class="form-group">
                <label for="detail_text">
                    Родительская категория
                </label>
                <select name="parent_id">
                    <option>
                    </option>
                    @foreach ($categories as $model)
                        <option @if($model->id == $category->parent_id) selected @endif
                                value="{{$model->id}}">

                            {{$model->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif

        <button type="submit" class="btn btn-primary">
            Сохранить
        </button>

    </form>

@endsection
