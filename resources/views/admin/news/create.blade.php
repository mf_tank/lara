@extends('adminlte::page')

@section('title', 'Создание новости')

@section('content_header')
    <h1>Создание новости</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('news.store') }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf

        <div class="form-group">
            <label for="name">
                Название
            </label>

            <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{old('name')}}"/>
        </div>

        <div class="form-group">
            <label for="preview_img">Картинка анонса</label>
            <input type="file" name="preview_img" class="form-control-file" id="preview_img">
        </div>

        <div class="form-group">
            <label for="preview_text">
                Текст анонса
            </label>
            <textarea class="form-control" id="preview_text" name="preview_text" placeholder="Анонс" >{{old('preview_text')}}</textarea>
        </div>

        <div class="form-group">
            <label for="detail_img">Детальная картинка</label>
            <input type="file" name="detail_img" class="form-control-file" id="detail_img">
        </div>

        <div class="form-group">
            <label for="detail_text">
                Детальный тект
            </label>
            <textarea  class="form-control" id="detail_text" name="detail_text" placeholder="Детальный текст" >{{old('detail_text')}}</textarea>
        </div>

        @if (count($categories) > 0)
            @php
                $config = [
                    'placeholder' => 'Выберите категорию',
                    'allowClear' => true
                ];
            @endphp
            <x-adminlte-select2 id="categories" name="categories[]" label="Категории"
                                size="sm" :config="$config" multiple>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">
                        {{$category->name}}
                    </option>
                @endforeach
            </x-adminlte-select2>

        @endif


        <button type="submit" class="btn btn-primary">
            Создать
        </button>

    </form>

@endsection
