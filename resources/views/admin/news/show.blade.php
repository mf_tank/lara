@extends('adminlte::page')

@section('title', 'Новость')

@section('content_header')
    <h1>Новость</h1>
@stop

@section('content')

    <h1>{{$news->name}}</h1>

    <h2>
        Картинка Анонса
    </h2>

    <div>
        <img width="200" src="{{$news->preview_img}}" />
    </div>

    <h2>
        Текст анонса
    </h2>

    <div>
        {{$news->preview_text}}
    </div>

    <h2>
        Картинка Детальная
    </h2>

    <div>
        <img width="200" src="{{$news->detail_img}}" />
    </div>

    <h2>
        Детальный текс
    </h2>

    <div>
        {{$news->preview_text}}
    </div>

@endsection
