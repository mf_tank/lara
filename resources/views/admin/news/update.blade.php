@extends('adminlte::page')

@section('title', 'Редактирование новости')

@section('content_header')
    <h1>Редактирование новости</h1>
@stop
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('news.update', $news->id) }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">
                Название
            </label>

            <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{$news->name}}"/>
        </div>

        <img width="200" src="{{$news->preview_img}}" />

        <div class="form-group">
            <label for="preview_img">Картинка анонса</label>
            <input type="file" name="preview_img" class="form-control-file" id="preview_img">
        </div>

        <div class="form-group">
            <label for="preview_text">
                Текст анонса
            </label>
            <textarea class="form-control" id="preview_text" name="preview_text" placeholder="Анонс" >{{$news->preview_text}}</textarea>
        </div>

        <img width="200" src="{{$news->detail_img}}" />

        <div class="form-group">
            <label for="detail_img">Детальная картинка</label>
            <input type="file" name="detail_img" class="form-control-file" id="detail_img">
        </div>

        <div class="form-group">
            <label for="detail_text">
                Детальный тект
            </label>
            <textarea  class="form-control" id="detail_text" name="detail_text" placeholder="Детальный текст" >{{$news->detail_text}}</textarea>
        </div>


        @if (count($categories) > 0)
            @php
                $config = [
                    'placeholder' => 'Выберите категорию',
                    'allowClear' => true
                ];
            @endphp
            <x-adminlte-select2 id="categories" name="categories[]" label="Категории"
                                size="sm" :config="$config" multiple>
                @foreach ($categories as $category)
                    <option @foreach($news->categories as $newsCategory) @if($newsCategory->id == $category->id) selected @endif @endforeach
                            value="{{$category->id}}">

                        {{$category->name}}
                    </option>
                @endforeach
            </x-adminlte-select2>

        @endif

        <button type="submit" class="btn btn-primary">
            Сохранить
        </button>

    </form>

@endsection
