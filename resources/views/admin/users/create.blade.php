@extends('adminlte::page')

@section('title', 'Создание пользователя')

@section('content_header')
    <h1>Создание пользователя</h1>
@stop



@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('users.store') }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf

        <div class="form-group">
            <label for="name">
                Имя
            </label>

            <input type="text" class="form-control" name="name" placeholder="Имя" value="{{old('name')}}"/>
        </div>

        <div class="form-group">
            <label for="name">
                Email
            </label>

            <input type="text" class="form-control" name="email" placeholder="Email" value="{{old('email')}}"/>
        </div>

        <div class="form-group">
            <label for="name">
                Пароль
            </label>

            <input type="password" class="form-control" name="password" placeholder="Пароль" value="{{old('password')}}"/>
        </div>

        <div class="form-group">
            <label for="name">
                Подтверждение пароля
            </label>

            <input type="password" class="form-control" name="password_confirmation" placeholder="Подтверждение пароля" value="{{old('password_confirmation')}}"/>
        </div>


        @if (count($roles) > 0)
            <h3>
                Роли
            </h3>
            @foreach ($roles as $key => $role)
                <div class="form-group">
                    <input type="checkbox" id="role{{$role->id}}" name="roles[]" value="{{$role->id}}"/>
                    <label for="role{{$role->id}}">
                        {{$role->name}}
                    </label>
                </div>
            @endforeach
        @endif


        <button type="submit" class="btn btn-primary">
            Создать
        </button>

    </form>

@endsection
