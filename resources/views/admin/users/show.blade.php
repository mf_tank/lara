@extends('adminlte::page')

@section('title', 'Просмотр пользователя')

@section('content_header')
    <h1>Просмотр пользователя</h1>
@stop



@section('content')

    <h3>
        Имя
    </h3>

    <div>
        {{$user->name}}
    </div>

    <h3>
        Email
    </h3>

    <div>
        {{$user->email}}
    </div>


    <h3>
        Роли
    </h3>

    <div>
        @foreach ($user->roles as $key => $role)
            {{$role->name}}@if($key != count($user->roles)-1),@endif
        @endforeach
    </div>

@endsection
