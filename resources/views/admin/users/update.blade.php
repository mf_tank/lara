@extends('adminlte::page')

@section('title', 'Редактирование пользователя')

@section('content_header')
    <h1>Редактирование пользователя</h1>
@stop

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('users.update', $user->id) }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">
                Имя
            </label>

            <input type="text" class="form-control" name="name" placeholder="Имя" value="{{$user->name}}"/>
        </div>

        <div class="form-group">
            <label for="name">
                Email
            </label>

            <input type="text" class="form-control" name="email" placeholder="Email" value="{{$user->email}}"/>
        </div>

        <div class="form-group">
            <label for="name">
                Пароль
            </label>

            <input type="password" class="form-control" name="password" placeholder="Пароль" />
        </div>

        <div class="form-group">
            <label for="name">
                Подтверждение пароля
            </label>

            <input type="password" class="form-control" name="password_confirmation" placeholder="Подтверждение пароля" />
        </div>

        <button type="submit" class="btn btn-primary">
            Создать
        </button>
    </form>


    <form action="{{ route('users.roles', $user->id) }}" method="POST" class="form" enctype="multipart/form-data">

        @csrf
        @method('PUT')

        @if (count($roles) > 0)
            <h3>
                Роли
            </h3>
            @foreach ($roles as $key => $role)
                <div class="form-group">
                    <input @foreach($user->roles as $userRole) @if($userRole->id == $role->id) checked @endif @endforeach type="checkbox" id="role{{$role->id}}" name="roles[]" value="{{$role->id}}"/>
                    <label for="role{{$role->id}}">
                        {{$role->name}}
                    </label>
                </div>
            @endforeach
        @endif


        <button type="submit" class="btn btn-primary">
            Создать
        </button>
    </form>


@endsection
