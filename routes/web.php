<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function() {

    Route::resource('users', 'App\Http\Controllers\Admin\UsersController');

    Route::put('users/roles/{user}', 'App\Http\Controllers\Admin\UsersController@roles')
        ->name('users.roles');

    Route::resource('news', 'App\Http\Controllers\Admin\NewsController');
    Route::resource('categories', 'App\Http\Controllers\Admin\CategoriesController');

});

Route::group(['prefix' => 'admin'], function() {

    Route::resource('profile', 'App\Http\Controllers\Admin\ProfileController')->only(['index']);

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
